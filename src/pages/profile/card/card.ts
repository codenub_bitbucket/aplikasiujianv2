import { Component } from '@angular/core';
import { LoadingController, NavController, NavParams, Platform, ViewController, AlertController } from 'ionic-angular';
import { GlobalProvider } from '../../../providers/global/global';
import { File } from '@ionic-native/file';
import { AndroidPermissions } from '@ionic-native/android-permissions';
import { FileTransfer, FileTransferObject } from '@ionic-native/file-transfer';

declare var cordova:any;

@Component({
  selector: 'page-card',
  templateUrl: 'card.html',
})
export class CardPage {
  token:any = this.global.getStorage("token");
  users:any = this.global.getStorage("users");
  siswa:any = this.global.getStorage("siswa");

  src:string;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public global: GlobalProvider,
    public viewCtrl : ViewController,
    public loadingCtrl : LoadingController,
    public file: File,
    private androidPermissions: AndroidPermissions,
    public platform: Platform,
    private transfer: FileTransfer,
    public alertCtrl : AlertController
  ) {

  }

  ionViewDidLoad() {
    // this.
    if(this.platform.is("cordova"))
    {
      this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE)
      this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.WRITE_EXTERNAL_STORAGE)
      const fileTransfer: FileTransferObject = this.transfer.create();
      fileTransfer.download(this.users.photo, cordova.file.dataDirectory + 'pp.jpg').then((entry) => {
        this.src = window['Ionic']['WebView'].convertFileSrc(entry.toURL());
        console.log('download complete: ' + this.src);
      }, (error) => {
        // handle error
      });
    }
    else
    {
      this.src = this.users.photo;
    }
  }

  download()
  {
    this.alertCtrl.create({
      message : "Feature Disabled!",
    }).present();
    // let loading = this.loadingCtrl.create({
    //   content: "Please wait...",
    // })
    // loading.present();
    // const div = document.getElementById("cardku");
    // const options = {background:"white",height :div.clientHeight , width : div.clientWidth  };
    // console.log("🚀 ~ file: card.ts ~ line 65 ~ CardPage ~ div", div)
    // html2canvas(div,options).then((canvas)=>{
    //   let imgData = canvas.toDataURL("image/PNG");
    //   console.log("🚀 ~ file: card.ts ~ line 55 ~ CardPage ~ html2canvas ~ imgData", imgData)
    // }).catch((e)=>{
    //   console.log(e);

    // });
    // let that = this;
    // try {
    //   html2canvas(document.querySelector("#cardku")).then(function(canvas) {
    //     let imgData = canvas.toDataURL("image/PNG");
    //     fetch(imgData,
    //     {
    //       method: "GET"
    //     }).then(res => res.blob()).then(blob => {
    //       that.file.writeFile(that.file.externalApplicationStorageDirectory, 'kartu-pelajar.jpg', blob, { replace: true }).then((s) => {
    //         that.global.toast('Kartu Pelajar Berhasil Disimpan! (Android/data/ujian.online.smkkartikatama/kartu-pelajar.jpg)')
    //         loading.dismiss();

    //       }).catch((e) => {
    //         that.global.toast('Kartu Pelajar Gagal Disimpan!')
    //         loading.dismiss();
    //       })
    //       // console.log("🚀 ~ file: card.ts ~ line 80 ~ CardPage ~ html2canvas ~ blob", blob)
    //       // that.file.checkDir(that.file.externalRootDirectory , 'SMKKARTIKATAMA/').then(_ => {
    //       // }).catch(err => {
    //       //   console.log("🚀 ~ file: card.ts ~ line 86 ~ CardPage ~ that.file.checkDir ~ err", err)
    //       //   that.file.createDir(that.file.externalRootDirectory , 'SMKKARTIKATAMA/',false).then(_ => {
    //       //     that.file.writeFile(that.file.externalRootDirectory + 'SMKKARTIKATAMA/', 'kp.jpg', blob, { replace: true }).then((s) => {
    //       //       console.log(s);

    //       //     }).catch((e) => {
    //       //       console.log(e);

    //       //     })
    //       //   }).catch(e => {
    //       //     console.log("🚀 ~ file: card.ts ~ line 109 ~ CardPage ~ that.file.createDir ~ e", e)

    //       //   })
    //       // });
    //     })
    //   });
    // } catch (error) {
    //   console.log(error);
    // }
  }

  //convert base64 to blob
  b64toBlob(b64Data, contentType) {
    contentType = contentType || '';
    var sliceSize = 512;
    var byteCharacters = atob(b64Data);
    var byteArrays = [];

    for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
      var slice = byteCharacters.slice(offset, offset + sliceSize);

      var byteNumbers = new Array(slice.length);
      for (var i = 0; i < slice.length; i++) {
        byteNumbers[i] = slice.charCodeAt(i);
      }

      var byteArray = new Uint8Array(byteNumbers);

      byteArrays.push(byteArray);
    }

    var blob = new Blob(byteArrays, {type: contentType});
    return blob;
  }


}
