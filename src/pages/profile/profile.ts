import { Component } from '@angular/core';
import { AlertController, LoadingController, ModalController, Nav, NavController, NavParams } from 'ionic-angular';
import { GlobalProvider } from '../../providers/global/global';
import { RestProvider } from '../../providers/rest/rest';
import { WelcomePage } from '../welcome/welcome';
import { CardPage } from './card/card';

@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage {

  token:any = this.global.getStorage("token");
  users:any = this.global.getStorage("users");
  siswa:any = this.global.getStorage("siswa");

  criteria:any = {
    api_token : this.token,
    users_id: this.users.id,
    siswa_id: this.siswa.siswa_serial_id,
  };

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public global: GlobalProvider,
    public rest : RestProvider,
    public loadingCtrl : LoadingController,
    public alertCtrl : AlertController,
    public nav : Nav,
    public modalCtrl : ModalController
    ) {
  }

  ionViewDidLoad()
  {
    // document.getElementById("content-profile").setAttribute("style", "margin-top: "+this.global.headerGlobalHeight+"px");
  }

  showCard()
  {
    let modal = this.modalCtrl.create(CardPage);
    modal.present();
  }

  logout()
  {
    let alert = this.alertCtrl.create({
      message: "Anda yakin untuk logout?",
      buttons: [
        {
          role: "cancel",
          text: "Tidak",
        },
        {
          role: "ok",
          text: "Iya",
          handler: () => {
            let loading = this.loadingCtrl.create({
              content : "Logging you out",
              enableBackdropDismiss : false,
            })

            loading.present();
            this.rest.logout(this.criteria).then((result) => {
              if(typeof result !== undefined && result['status'] == 200)
              {
                this.global.toast("Berhasil keluar!");

                setTimeout(() => {
                  this.global.isLogin.emit(false);
                  this.global.clearStorage();
                  loading.dismiss();
                  this.nav.setRoot(WelcomePage);
                }, 2000);
              }
              else
              {
                setTimeout(() => {
                  this.global.isLogin.emit(false);
                  this.global.clearStorage();
                  loading.dismiss();
                  this.nav.setRoot(WelcomePage);
                }, 2000);
              }
            })
          }
        }
      ]
    })
    alert.present();
  }
}
