import { Component } from '@angular/core';
import { NavController, Platform } from 'ionic-angular';
import { GlobalProvider } from '../../providers/global/global';
import { BerandaPage } from '../beranda/beranda';
import { ELearningPage } from '../e-learning/e-learning';
import { ExamPage } from '../exam/exam';
import { ProfilePage } from '../profile/profile';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  users:any = this.global.getStorage("users");
  siswa:any = this.global.getStorage("siswa");
  tabs = {
    beranda : BerandaPage,
    exam : ExamPage,
    profile : ProfilePage,
    elearning : ELearningPage,
  }

  src:string = ''

  isLogin:boolean = false;

  constructor(
    public navCtrl: NavController,
    public global : GlobalProvider,
    public platform: Platform
  ) {

    // if(this.platform.is("cordova"))
    // {
    //   this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.LANDSCAPE);
    // }
    // this.global.isLogin.subscribe(data => {
    //   this.isLogin = data;
    // })
  }

  // download()
  // {
  //   var node = document.getElementById('header-global');
  //   let that = this;
  //   domtoimage.toPng(node)
  //   .then(function (dataUrl) {
  //       var img = new Image();
  //       img.src = dataUrl;
  //       console.log(img);
  //       that.src = img.src;
  //   })
  //   .catch(function (error) {
  //       console.error('oops, something went wrong!', error);
  //   });
  // }

}
