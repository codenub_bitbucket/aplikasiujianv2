import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { GlobalProvider } from '../../providers/global/global';
import { BerandaParentPage } from '../beranda-parent/beranda-parent';
import { HomePage } from '../home/home';
import { LoginPage } from '../login/login';

@Component({
  selector: 'page-welcome',
  templateUrl: 'welcome.html',
})
export class WelcomePage {

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public global: GlobalProvider,
  ) {

  }

  ionViewWillLoad()
  {
    let version = this.global.getStorage('version');
    console.log("🚀 ~ file: welcome.ts ~ line 25 ~ WelcomePage ~ version", version, this.global.config('versionApp'))
    if(version == null || version !== this.global.config('versionApp'))
    {
      this.global.clearStorage();
      this.global.setStorage('version', this.global.config('versionApp'));
    }
    else
    {
      let session = this.global.getStorage("is_login");
      let parent = this.global.getStorage("isParent");
      if(session == true)
      {
        if (parent == true)
        {
          this.navCtrl.setRoot(BerandaParentPage);
        }
        else
        {
          this.navCtrl.setRoot(HomePage);
        }
      }
    }

  }

  login() {
    this.navCtrl.setRoot(LoginPage);
  }

}
