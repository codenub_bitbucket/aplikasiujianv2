import { Component } from '@angular/core';
import { LoadingController, NavController, NavParams } from 'ionic-angular';
import { GlobalProvider } from '../../providers/global/global';
import { RestProvider } from '../../providers/rest/rest';
import { BerandaParentPage } from '../beranda-parent/beranda-parent';
import { HomePage } from '../home/home';

@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  data:any = {
    email : '',
    password : '',
  }

  typeInput="password";
  isShowPassword:boolean = false;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public rest : RestProvider,
    public global: GlobalProvider,
    public loadingCtrl: LoadingController,
  ) {
    let session = this.global.getStorage("is_login");
    let parent = this.global.getStorage("isParent");
    console.log("🚀 ~ file: login.ts ~ line 26 ~ LoginPage ~ session", session)
    if(session == true)
    {
      if (parent)
      {
        this.navCtrl.setRoot(BerandaParentPage);
      }
      else
      {
        this.navCtrl.setRoot(HomePage);
      }
    }
  }

  showHidePassword()
  {
    this.isShowPassword = !this.isShowPassword;

    if(this.isShowPassword)
    {
      this.typeInput = "text";
    }
    else
    {
      this.typeInput = "password";
    }
  }

  login() {
    let loading = this.loadingCtrl.create({
      content: "Loading...",
      spinner: "crescent"
    })

    loading.present();

    this.rest.auth(this.data)
    .then((result) => {
      loading.dismiss();

      if(typeof result !== undefined && result['status'] == 200)
      {
        if(result['data']['data_wali'] !== undefined)
        {
          this.global.toast(result['msg']);
          this.global.setStorage("is_login", true);
          this.global.isLogin.emit(true);
          this.global.setStorage("isParent", true);
          this.global.setStorage("wali", result['data']['data_wali']);
          this.global.setStorage("siswa", result['data']['data_siswa']);
          this.navCtrl.setRoot(BerandaParentPage)
        }
        else
        {
          this.global.toast(result['msg']);
          this.global.setStorage("is_login", true);
          this.global.isLogin.emit(true);
          this.global.setStorage("users", result['data']['users']);
          this.global.setStorage("siswa", result['data']['siswa']);
          this.global.setStorage("token", result['data']['users']['api_token']);
          this.navCtrl.setRoot(HomePage);
        }
      }
      else
      {
        this.global.toast(result['msg']);
      }
    })

  }
}
