import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import moment from 'moment';
import { GlobalProvider } from '../../providers/global/global';
import { RestProvider } from '../../providers/rest/rest';

@Component({
  selector: 'page-beranda',
  templateUrl: 'beranda.html',
})
export class BerandaPage {

  token:any = this.global.getStorage("token");
  users:any = this.global.getStorage("users");
  siswa:any = this.global.getStorage("siswa");

  criteria:any = {
    api_token: this.token,
    show: 15,
    users_id: this.users.id,
    siswa_id: this.siswa.siswa_serial_id,
  }

  timeText:string = "Datang";
  iconTime:string = "";
  serverTime:string = "00:00";

  listData:any = new Array();

  loadingData:boolean = true;

  constructor(
    public navCtrl: NavController,
    public global: GlobalProvider,
    public navParams: NavParams,
    public rest : RestProvider,
  ) {
    this.initServerTime(moment().utcOffset("+0700"));
    this.getExam();
  }

  ionViewDidLoad()
  {
    // console.log('load');
    // document.getElementById("content-beranda").setAttribute("style", "margin-top: "+this.global.headerGlobalHeight+"px");
  }

  getExam(refresher=undefined)
  {
    this.rest.listData('ujian', this.criteria)
    .then((result) => {
      if(refresher !== undefined)
      {
        refresher.complete();
      }
      this.loadingData = false;
      if(typeof result !== undefined && result['status'] == 200)
      {
        this.listData = result['data']['data'];
      }
      else
      {
        this.global.toast(result['msg']);
      }
    })
  }

  initServerTime(currentTime:any)
  {
    this.serverTime = moment().utcOffset("+0700").format("HH:mm");
    setTimeout(() => {
      const splitAfternoon = 11; // 24hr time to split the afternoon
      const splitEvening = 15; // 24hr time to split the evening
      const splitNight = 18; // 24hr time to split the night
      const currentHour = parseFloat(currentTime.format('HH'));

      if (currentHour >= splitAfternoon && currentHour < splitEvening) {
        this.timeText = 'Siang';
        this.iconTime = 'mdi-white-balance-sunny';
      } else if (currentHour >= splitEvening && currentHour < splitNight) {
        this.timeText = 'Sore';
        this.iconTime = 'mdi-weather-sunset-down';
      } else if (currentHour >= splitNight) {
        this.timeText = 'Malam';
        this.iconTime = 'mdi-weather-night';
      } else {
        this.timeText = 'Pagi';
        this.iconTime = 'mdi-weather-sunset';
      }

      this.initServerTime(moment().utcOffset("+0700"));
    }, 1000);
  }

  doRefresh(refresher)
  {
    this.getExam(refresher);
  }

}
