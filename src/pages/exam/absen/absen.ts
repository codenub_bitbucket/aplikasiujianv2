import { Component } from '@angular/core';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { NavController, NavParams, ViewController, LoadingController } from 'ionic-angular';
import { GlobalProvider } from '../../../providers/global/global';
import { RestProvider } from '../../../providers/rest/rest';

@Component({
  selector: 'page-exam',
  templateUrl: 'absen.html',
})
export class AbsenPage {

  content:any = {};
  ujian:any = {};
  base64Image;

  loadingData;

  constructor(
    public rest : RestProvider,
    public global: GlobalProvider,
    public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl : ViewController,
    public camera: Camera,
    public loadingCtrl : LoadingController,
  ) {
    this.content = this.navParams.get('data');
    this.ujian = this.navParams.get('ujian');
    console.log(this.ujian, this.content);

  }

  doAbsen()
  {
    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      sourceType: 1,
      cameraDirection: 1
    }

    this.camera.getPicture(options).then((imageData) => {
     this.base64Image = 'data:image/jpeg;base64,' + imageData;
    }, (err) => {
     // Handle error
    });

  }

  saveAbsen()
  {
    this.loadingData = this.loadingCtrl.create({
      content: "Melakukan Absen..."
    });
    let cont = {
      'users' : this.content,
      'ujian' : this.ujian,
      'absen' : this.base64Image
    }
    this.loadingData.present();
    this.rest.saveAbsen(cont).then((result:any) => {
      this.loadingData.dismissAll();
      if(typeof result !== undefined && result['status'] == 200)
      {
        this.viewCtrl.dismiss(true);
      }
      else
      {
        this.global.toast(result['msg']);
      }
    })
  }


}
