import { Component } from '@angular/core';
import { AlertController, LoadingController, ModalController, NavController, NavParams, ViewController } from 'ionic-angular';
import { GlobalProvider } from '../../../providers/global/global';
import { RestProvider } from '../../../providers/rest/rest';
import { FinishPage } from '../finish/finish';

@Component({
  selector: 'page-exam',
  templateUrl: 'exam_preview.html',
})
export class ExamPreviewPage {

  token:any = this.global.getStorage("token");
  users:any = this.global.getStorage("users");
  siswa:any = this.global.getStorage("siswa");

  criteria:any = {
    api_token: this.token,
    show: 15,
    users_id: this.users.id,
    siswa_id: this.siswa.siswa_serial_id,
  }

  content:any = {};

  currentNumber:number = 1;
  isFinished:boolean = false;
  btnPrev:boolean = false;
  btnNext:boolean = true;

  uuid: string = "";

  loadingData:boolean = true;

  data:any = {
    api_token : this.token,
    users_id: this.users.id,
    siswa_id: this.siswa.siswa_serial_id,
    answer : {},
  };

  subTime:any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController,
    public modalCtrl : ModalController,
    public global: GlobalProvider,
    public rest: RestProvider,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController
  ) {
    this.uuid = this.navParams.get('uuid');

    this.getDetail();

    this.subTime = this.global.stopUjian.subscribe(data => {
      if(data == this.uuid)
      {
        this.saveData("Waktu ujian habis, menyimpan hasil ujian...")
      }
    })

  }

  ionViewDidLoad()
  {
    // setTimeout(() => {
    //   let footer:any = document.getElementById('footer-exam-preview');
    //   footer = footer.offsetHeight;
    //   document.getElementById('list-choices').setAttribute('style', 'padding-bottom: '+ footer +"px");
    // }, 1000)
  }

  setCacheAnswer()
  {
    this.data.answer = (this.global.getStorage(this.content.ujian_uuid) !== null) ? this.global.getStorage(this.content.ujian_uuid) : {};
  }

  getDetail()
  {
    this.criteria.uuid = this.uuid;
    this.rest.getDetail('ujian', this.criteria).then((result) => {
      this.loadingData = false;
      if(typeof result !== undefined && result['status'] == 200)
      {
        this.content = result['data'];
        this.data.ujian_serial_id = this.content.ujian_serial_id;

        this.global.startTimer(this.content.ujian_duration, this.content.ujian_uuid)

        this.ionViewDidLoad();
        this.proceedChoices();
        this.setCacheAnswer();

      }
      else
      {
        this.global.toast(result['msg']);
      }
    })
  }

  proceedChoices()
  {
    this.content.soal.forEach((item, key) => {
      let finCo = [];
      let temChoi = JSON.parse(item.soal_choices);
      Object.keys(temChoi).forEach(k => {
        finCo.push({
          value : k,
          label : temChoi[k]
        });
        this.content.soal[key]['soal_choices'] = finCo;
      })
    });
  }

  setAnswer(choice, index)
  {
    this.data.answer[index] = choice;
    this.global.setStorage(this.content.ujian_uuid, this.data.answer);
  }

  next()
  {
    if (this.data.answer[this.content.soal[this.currentNumber-1].soal_uuid] == undefined || this.data.answer[this.content.soal[this.currentNumber-1].soal_uuid] == null)
    {
      this.data.answer[this.content.soal[this.currentNumber-1].soal_uuid] = null;
    }

    if (this.currentNumber == this.content.soal.length)
    {
      this.isFinished = true;
    }
    else
    {
      this.btnPrev = true;
      this.currentNumber++;
    }
  }

  prev()
  {
    if (this.currentNumber == 1)
    {
      this.btnPrev = false;
    }
    else
    {
      this.currentNumber--;
    }
  }

  finish()
  {
    let alert = this.alertCtrl.create({
      message: "Anda yakin untuk menyelesaikan soal ini?",
      buttons: [
        {
          role: "cancel",
          text: "Tidak",
        },
        {
          role: "ok",
          text: "Iya",
          handler: () => {
            this.saveData();
          }
        }
      ]
    })
    alert.present();
  }

  saveData(msg="Menyimpan hasil ujian...")
  {
    let loading = this.loadingCtrl.create({
      enableBackdropDismiss : false,
      content : msg,
      spinner : "crescent"
    })

    loading.present();

    this.data['answer'] = this.proceedAnswerBeforeSave(this.data.answer);

    this.rest.saveData('ujian', this.data).then((result) => {
      loading.dismiss();
      if(typeof result !== undefined && result['status'] == 200)
      {
        this.global.stopTimer(this.global.getStorage("interval_"+this.content.ujian_uuid))
        this.subTime.unsubscribe();
        this.global.removeStorage(this.content.ujian_uuid);
        this.global.removeStorage("timer_"+this.content.ujian_uuid);
        this.global.removeStorage("interval_"+this.content.ujian_uuid);
        this.navCtrl.setRoot(FinishPage, {uuid : this.content.ujian_uuid });
        this.global.toast("Hasil ujian berhasil disimpan!");
      }
      else
      {
        this.global.toast(result['msg']);
      }
    })
  }

  proceedAnswerBeforeSave(data)
  {
    let answer = data;
    this.content.soal.forEach(item => {
      if(answer[item['soal_uuid']] == undefined)
      {
        answer[item['soal_uuid']] = null;
      }
    });

    return answer;
  }
}
