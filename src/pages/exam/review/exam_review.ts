import { Component } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';
import { GlobalProvider } from '../../../providers/global/global';
import { RestProvider } from '../../../providers/rest/rest';

@Component({
  selector: 'page-exam',
  templateUrl: 'exam_review.html',
})
export class ExamReviewPage {

  content:any = {};

  constructor(
    public rest : RestProvider,
    public global: GlobalProvider,
    public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl : ViewController
    ) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProfilePage');
  }

}
