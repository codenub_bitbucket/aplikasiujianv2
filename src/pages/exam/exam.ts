import { Component } from '@angular/core';
import { AlertController, ModalController, NavController, NavParams, LoadingController } from 'ionic-angular';
import { GlobalProvider } from '../../providers/global/global';
import { RestProvider } from '../../providers/rest/rest';
import { ExamPreviewPage } from './preview/exam_preview';
import { AbsenPage } from './absen/absen';
import moment from 'moment';

@Component({
  selector: 'page-exam',
  templateUrl: 'exam.html',
})
export class ExamPage {

  token:any = this.global.getStorage("token");
  users:any = this.global.getStorage("users");
  siswa:any = this.global.getStorage("siswa");

  criteria:any = {
    api_token: this.token,
    show: 15,
    users_id: this.users.id,
    siswa_id: this.siswa.siswa_serial_id,
  }

  timeText:string = "Datang";
  iconTime:string = "";
  serverTime:string = "00:00";

  listData:any = new Array();

  loadingData:boolean = true;
  loadingDataAbsen;

  absenData:any= false;

  constructor(
    public navCtrl: NavController,
    public global: GlobalProvider,
    public navParams: NavParams,
    public rest : RestProvider,
    public modalCtrl : ModalController,
    public alertCtrl : AlertController,
    public loadingCtrl : LoadingController,
  ) {
    this.getExam();
    // this.getAbsen();
  }

  ionViewDidLoad()
  {
    // document.getElementById("content-exam").setAttribute("style", "margin-top: "+this.global.headerGlobalHeight+"px");
  }

  getExam(refresher=undefined)
  {
    this.rest.listData('ujian', this.criteria)
    .then((result) => {
      if(refresher !== undefined)
      {
        refresher.complete();
      }
      this.loadingData = false;
      if(typeof result !== undefined && result['status'] == 200)
      {
        this.listData = result['data']['data'];
      }
      else
      {
        this.global.toast(result['msg']);
      }
    })
  }

  getAbsen(uuid)
  {
    return new Promise(resolve => {
      this.criteria['ujian_uuid'] = uuid;
      this.rest.getAbsenStatus(this.criteria)
      .then((result) => {
        resolve(result);
      }).catch(e => {
        resolve(false);
      })
    })
  }

  doRefresh(refresher)
  {
    this.getExam(refresher)
  }

  doExam(uuid, status, ttlSoal, ujian)
  {
    let load = this.loadingCtrl.create({
      content : "Mohon Tunggu...",
    })

    load.present();

    const start = moment(ujian.ujian_start_access, "hh:mm").toDate();

    this.rest.getServerTime(this.criteria).then((result:any) => {
      load.dismiss();
      if(typeof result !== undefined && result['status'] == 200)
      {
        let current = moment(result.data, "hh:mm").toDate();

        var a = moment(start);
        var b = moment(current);
        let as = a.diff(b, 'minutes') // 1
        console.log(as, a, b);

        if(as >= 1)
        {
          return this.global.toast("Ujian Baru Bisa Dikerjakan "+as+" Menit Lagi!")
        }

        if (ttlSoal > 0)
        {
          if(status)
          {
            this.global.toast("Ujian sudah dikerjakan!");
          }
          else
          {
            this.loadingDataAbsen = this.loadingCtrl.create({
              content : "Check Absen, Mohon Tunggu...",
            })

            this.loadingDataAbsen.present();

            this.getAbsen(uuid).then((ab:any) => {
              this.loadingDataAbsen.dismiss()

              if (ab !== false && ab.is_absen == false)
              {
                let absen = this.alertCtrl.create({
                  title : "Foto Kehadiran",
                  message : "Silahkan foto kehadiran untuk melanjutkan",
                  buttons : [
                    {
                      text : "Absen",
                      handler : () => {
                        this.doAbsenModal(ujian);
                      }
                    },
                    {
                      text : "Batal",
                      role : "cancel"
                    }
                  ]
                })

                absen.present();
              }
              else
              {
                if(ab.data.is_approve == 0 && ab.data.is_decline == 0)
                {
                  return this.global.toast("Absen Anda Belum Di Approve, Silahkan Tunggu Admin Me-Review Absen Anda!");
                }
                else if(ab.data.is_decline == 1)
                {
                  this.global.toast("Absen Anda DiTolak, Silahkan Melakukan Absen Kembali!");
                  let absen = this.alertCtrl.create({
                    title : "Foto Kehadiran",
                    message : "Silahkan foto kehadiran untuk melanjutkan",
                    buttons : [
                      {
                        text : "Absen",
                        handler : () => {
                          this.doAbsenModal(ujian);
                        }
                      },
                      {
                        text : "Batal",
                        role : "cancel"
                      }
                    ]
                  })

                  absen.present();
                }
                else
                {
                  let enterToken = this.alertCtrl.create({
                    message: "Masukan token ujian anda!",
                    inputs: [
                      {
                        name: 'token',
                        placeholder: 'Token ujian'
                      }
                    ],
                    buttons: [
                      {
                        role: "cancel",
                        text: "Batal",
                      },
                      {
                        role: "ok",
                        text: "Submit",
                        handler: data => {
                          let token = data.token;
                          let siswa_token = this.siswa.siswa_token;
                          if(token == siswa_token)
                          {
                            let alert = this.alertCtrl.create({
                              message: "Anda yakin ingin mengerjakan soal ini?",
                              buttons: [
                                {
                                  role: "cancel",
                                  text: "Tidak",
                                },
                                {
                                  role: "ok",
                                  text: "Iya",
                                  handler: () => {
                                    let modal = this.modalCtrl.create(ExamPreviewPage, {uuid: uuid});

                                    modal.onDidDismiss(data => {
                                      this.getExam();
                                    })

                                    modal.present();
                                  }
                                }
                              ]
                            })
                            alert.present();
                          }
                          else
                          {
                            this.global.toast("Token anda salah!");
                          }
                        }
                      }
                    ]
                  })
                  enterToken.present();
                }
              }

            });
          }
        }
        else
        {
          return this.global.toast("Belum Ada Soal!");
        }
      }
      else
      {
        this.global.toast("ERROR");
      }
    });
  }

  doAbsenModal(ujian)
  {
    let modal = this.modalCtrl.create(AbsenPage, {data : this.criteria, ujian : ujian})
    modal.present();

    modal.onDidDismiss((data) => {
      if(data == true)
      {
        this.global.toast("Absen Telah Disubmit. Silahkan Tunggu Admin Untuk Review Absen Anda.")
      }
    })
  }

}
