import { Component } from '@angular/core';
import { ModalController, NavController, NavParams, ViewController } from 'ionic-angular';
import { GlobalProvider } from '../../../providers/global/global';
import { HomePage } from '../../home/home';
import { ExamReviewPage } from '../review/exam_review';

@Component({
  selector: 'page-exam',
  templateUrl: 'finish.html',
})
export class FinishPage {

  users:any = this.global.getStorage("users");
  siswa:any = this.global.getStorage("siswa");

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl : ViewController,
    public modalCtrl : ModalController,
    public global: GlobalProvider,

  ) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProfilePage');
  }

  home()
  {
    this.navCtrl.setRoot(HomePage);
  }

  review()
  {
    this.modalCtrl.create(ExamReviewPage).present();
  }

}
