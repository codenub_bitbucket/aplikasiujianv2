import { Component } from '@angular/core';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { AlertController, Nav } from 'ionic-angular';
import { NavController, NavParams } from 'ionic-angular';
import { GlobalProvider } from '../../providers/global/global';
import { WelcomePage } from '../welcome/welcome';
import { DashboardPage } from './dashboard/dashboard';
import { PengumumanPage } from './pengumuman/pengumuman';
import { SettingPage } from './setting/setting';

@Component({
  selector: 'page-beranda-parent',
  templateUrl: 'beranda-parent.html',
})
export class BerandaParentPage {
  tabs:any = {
    tab1 : DashboardPage,
    tab2 : PengumumanPage,
    tab3 : SettingPage
  }

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public global: GlobalProvider,
    public iab : InAppBrowser,
    public nav : Nav,
    public alertCtrl : AlertController,
  ) {

  }



  logout()
  {
    let alert = this.alertCtrl.create({
      message: "Anda yakin untuk logout?",
      buttons: [
        {
          role: "cancel",
          text: "Tidak",
        },
        {
          role: "ok",
          text: "Iya",
          handler: () => {
            this.global.isLogin.emit(false);
            this.global.clearStorage();
            this.nav.setRoot(WelcomePage);
          }
        }
      ]
    })
    alert.present();
  }

  contact() {
    this.iab.create('https://api.whatsapp.com/send?phone=6285266849660', '_system');
  }

}
