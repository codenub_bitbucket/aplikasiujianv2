import { Component } from '@angular/core';
import { FileOpener } from '@ionic-native/file-opener';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { AlertController, LoadingController, ModalController, NavController, NavParams, Platform, ViewController } from 'ionic-angular';
import { GlobalProvider } from '../../../providers/global/global';
import { RestProvider } from '../../../providers/rest/rest';
import moment from 'moment';

@Component({
  selector: 'page-beranda-parent',
  templateUrl: 'dashboard.html',
})
export class DashboardPage {

  wali = this.global.getStorage("wali");
  siswa = this.global.getStorage("siswa");

  today = moment().locale('id').format('dddd');
  todayDate = moment().locale('id').format('ll');

  absence = null;
  statusAbsence = "Masuk";

  timeText:string = "Datang";
  iconTime:string = "";
  serverTime:string = "00:00";

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController,
    public modalCtrl : ModalController,
    public global: GlobalProvider,
    public rest: RestProvider,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    public iab : InAppBrowser,
    public platform : Platform,
    public fileOpener : FileOpener,
  ) {
    if(this.siswa.siswa_absence !== undefined)
    {
      this.absence = this.siswa.siswa_absence;
    }
    if(this.absence == null)
    {
      this.statusAbsence = "Masuk";
    }
    else
    {
      let absence = this.absence;
      if(absence[moment().format('YYYY-MM-DD')] == false)
      {
        this.statusAbsence = "Tidak Masuk";
      }
      else if(absence[moment().format('YYYY-MM-DD')] == true)
      {
        this.statusAbsence = "Masuk";
      }
      else
      {
        this.statusAbsence = "Masuk";
      }
    }

    this.initServerTime(moment().utcOffset("+0700"));
  }

  initServerTime(currentTime:any)
  {
    this.serverTime = moment().utcOffset("+0700").format("HH:mm");
    setTimeout(() => {
      const splitAfternoon = 11; // 24hr time to split the afternoon
      const splitEvening = 15; // 24hr time to split the evening
      const splitNight = 18; // 24hr time to split the night
      const currentHour = parseFloat(currentTime.format('HH'));

      if (currentHour >= splitAfternoon && currentHour < splitEvening) {
        this.timeText = 'Siang';
        this.iconTime = 'mdi-white-balance-sunny';
      } else if (currentHour >= splitEvening && currentHour < splitNight) {
        this.timeText = 'Sore';
        this.iconTime = 'mdi-weather-sunset-down';
      } else if (currentHour >= splitNight) {
        this.timeText = 'Malam';
        this.iconTime = 'mdi-weather-night';
      } else {
        this.timeText = 'Pagi';
        this.iconTime = 'mdi-weather-sunset';
      }

      this.initServerTime(moment().utcOffset("+0700"));
    }, 1000);
  }


}
