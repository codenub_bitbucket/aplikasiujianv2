import { Component } from '@angular/core';
import { FileOpener } from '@ionic-native/file-opener';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { AlertController, LoadingController, ModalController, Nav, NavController, NavParams, Platform, ViewController } from 'ionic-angular';
import { GlobalProvider } from '../../../providers/global/global';
import { RestProvider } from '../../../providers/rest/rest';
import { WelcomePage } from '../../welcome/welcome';

@Component({
  selector: 'page-beranda-parent',
  templateUrl: 'setting.html',
})
export class SettingPage {

  token:any = this.global.getStorage("token");
  users:any = this.global.getStorage("users");
  siswa:any = this.global.getStorage("siswa");

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController,
    public modalCtrl : ModalController,
    public global: GlobalProvider,
    public rest: RestProvider,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    public iab : InAppBrowser,
    public platform : Platform,
    public fileOpener : FileOpener,
    public nav : Nav,

  ) {
  }

  logout()
  {
    let alert = this.alertCtrl.create({
      message: "Anda yakin untuk logout?",
      buttons: [
        {
          role: "cancel",
          text: "Tidak",
        },
        {
          role: "ok",
          text: "Iya",
          handler: () => {
            this.global.isLogin.emit(false);
            this.global.clearStorage();
            this.nav.setRoot(WelcomePage);
          }
        }
      ]
    })
    alert.present();
  }

  contact() {
    this.iab.create('https://api.whatsapp.com/send?phone=6285266849660', '_system');
  }

}
