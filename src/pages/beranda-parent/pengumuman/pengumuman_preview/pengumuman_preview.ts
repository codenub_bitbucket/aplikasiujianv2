import { Component } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';
import { GlobalProvider } from '../../../../providers/global/global';
import { RestProvider } from '../../../../providers/rest/rest';

@Component({
  selector: 'page-beranda-parent',
  templateUrl: 'pengumuman_preview.html',
})
export class PengumumanPreviewPage {

  token:any = this.global.getStorage("token");
  users:any = this.global.getStorage("users");
  siswa:any = this.global.getStorage("siswa");

  content:any = new Array();

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController,
    public rest : RestProvider,
    public global: GlobalProvider,
  ) {

    this.content = this.navParams.get('data');

    console.log(this.content);
  }


}
