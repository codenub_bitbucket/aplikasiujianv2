import { Component } from '@angular/core';
import { FileOpener } from '@ionic-native/file-opener';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { AlertController, LoadingController, ModalController, NavController, NavParams, Platform, ViewController } from 'ionic-angular';
import { GlobalProvider } from '../../../providers/global/global';
import { RestProvider } from '../../../providers/rest/rest';
import { PengumumanPreviewPage } from './pengumuman_preview/pengumuman_preview';

@Component({
  selector: 'page-beranda-parent',
  templateUrl: 'pengumuman.html',
})
export class PengumumanPage {

  token:any = this.global.getStorage("token");
  users:any = this.global.getStorage("users");
  siswa:any = this.global.getStorage("siswa");

  criteria:any = {
    isParent: true,
    show: 15,
  }

  listData:any = new Array();

  loadingData:boolean = true;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController,
    public modalCtrl : ModalController,
    public global: GlobalProvider,
    public rest: RestProvider,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    public iab : InAppBrowser,
    public platform : Platform,
    public fileOpener : FileOpener,
  ) {
    this.getData();
  }

  getData(refresher=undefined)
  {
    this.rest.listData('pengumuman', this.criteria)
    .then((result) => {
      if(refresher !== undefined)
      {
        refresher.complete();
      }
      this.loadingData = false;
      if(typeof result !== undefined && result['status'] == 200)
      {
        this.listData = result['data']['data'];
      }
      else
      {
        this.global.toast(result['msg']);
      }
    })
  }

  doRefresh(refresher)
  {
    this.getData(refresher);
  }

  detail(data)
  {
    let modal = this.modalCtrl.create(PengumumanPreviewPage, {data : data});

    modal.present();
  }


}
