import { Component } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { FileOpener } from '@ionic-native/file-opener';
import { FileTransfer, FileTransferObject } from '@ionic-native/file-transfer';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { AlertController, LoadingController, ModalController, NavController, NavParams, Platform, ViewController } from 'ionic-angular';
import { GlobalProvider } from '../../../providers/global/global';
import { RestProvider } from '../../../providers/rest/rest';
import { File } from '@ionic-native/file';

@Component({
  selector: 'page-e-learning',
  templateUrl: 'detail.html',
})
export class ElearningDetailPage {

  token:any = this.global.getStorage("token");
  users:any = this.global.getStorage("users");
  siswa:any = this.global.getStorage("siswa");

  criteria:any = {
    api_token: this.token,
    show: 15,
    users_id: this.users.id,
    siswa_id: this.siswa.siswa_serial_id,
  }

  content:any = {};

  uuid: string = "";

  loadingData:boolean = true;

  pdfLink:any;

  iabRef;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController,
    public modalCtrl : ModalController,
    public global: GlobalProvider,
    public rest: RestProvider,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    private sanitizer: DomSanitizer,
    public iab : InAppBrowser,
    public platform : Platform,
    public fileOpener : FileOpener,
    private transfer: FileTransfer,
    private file: File
  ) {
    this.uuid = this.navParams.get('uuid');

    this.getDetail();
  }

  getDetail()
  {
    this.criteria.uuid = this.uuid;
    this.rest.getDetail('elearning', this.criteria).then((result) => {
      this.loadingData = false;
      if(typeof result !== undefined && result['status'] == 200)
      {
        this.content = result['data'];
        this.pdfLink = this.sanitizer.bypassSecurityTrustResourceUrl(this.content.elearning_file);
      }
      else
      {
        this.global.toast(result['msg']);
      }
    })
  }

  openPDF(link)
  {
    let loading = this.loadingCtrl.create({
      content : "Loading..."
    });


    if (this.platform.is('cordova') && this.platform.is('android')) {
      loading.present();
      const fileTransfer: FileTransferObject = this.transfer.create();

      fileTransfer.download(link, this.file.dataDirectory + 'file.pdf').then((entry) => {
        loading.dismiss();
        console.log('download complete: ' + entry.toURL());
        this.fileOpener.open(entry.toURL(), 'application/pdf')
        .then(() => console.log('File is opened'))
        .catch(e => console.log('Error opening file', e));
      }, (error) => {
        // handle error
      });
    } else {
      this.iab.create(link, "_blank");
    }
  }

  dismiss()
  {
    this.viewCtrl.dismiss();
  }
}
