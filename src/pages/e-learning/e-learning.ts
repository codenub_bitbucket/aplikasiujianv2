import { Component } from '@angular/core';
import { AlertController, ModalController } from 'ionic-angular';
import { NavController, NavParams } from 'ionic-angular';
import { GlobalProvider } from '../../providers/global/global';
import { RestProvider } from '../../providers/rest/rest';
import { ElearningDetailPage } from './preview/detail';

@Component({
  selector: 'page-e-learning',
  templateUrl: 'e-learning.html',
})
export class ELearningPage {

  token:any = this.global.getStorage("token");
  users:any = this.global.getStorage("users");
  siswa:any = this.global.getStorage("siswa");

  criteria:any = {
    api_token: this.token,
    show: 15,
    users_id: this.users.id,
    siswa_id: this.siswa.siswa_serial_id,
  }

  listData:any = new Array();

  loadingData:boolean = true;

  constructor(
    public navCtrl: NavController,
    public global: GlobalProvider,
    public navParams: NavParams,
    public rest : RestProvider,
    public modalCtrl : ModalController,
    public alertCtrl : AlertController,
  ) {
    this.getElearning();
  }

  ionViewDidLoad()
  {
    // document.getElementById("content-elearning").setAttribute("style", "margin-top: "+this.global.headerGlobalHeight+"px");
  }

  getElearning(refresher=undefined)
  {
    this.rest.listData('elearning', this.criteria)
    .then((result) => {
      if(refresher !== undefined)
      {
        refresher.complete();
      }
      this.loadingData = false;
      if(typeof result !== undefined && result['status'] == 200)
      {
        this.listData = result['data']['data'];
      }
      else
      {
        this.global.toast(result['msg']);
      }
    })
  }

  doRefresh(refresher)
  {
    this.getElearning(refresher)
  }

  detail(uuid)
  {
    let modal = this.modalCtrl.create(ElearningDetailPage, {uuid: uuid});

    modal.onDidDismiss(data => {
      this.getElearning();
    })

    modal.present();
  }
}
