import { Component, Input } from '@angular/core';
import { GlobalProvider } from '../../providers/global/global';

@Component({
  selector: 'header-global',
  templateUrl: 'header-global.html'
})
export class HeaderGlobalComponent {

  @Input('siswa') siswa:any = {};
  @Input('users') users:any = {};

  constructor(
    public global : GlobalProvider
  ) {
  }

  ngOnInit() {
    console.log('init');
    // this.global.headerGlobalHeight = document.getElementById("header-global").offsetHeight;
  }

}
