import { NgModule } from '@angular/core';
import { IonicModule } from 'ionic-angular';
import { HeaderGlobalComponent } from './header-global/header-global';
@NgModule({
	declarations: [HeaderGlobalComponent],
	imports: [IonicModule],
	exports: [HeaderGlobalComponent]
})
export class ComponentsModule {}
