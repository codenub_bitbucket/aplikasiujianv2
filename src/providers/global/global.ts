import { HttpClient } from '@angular/common/http';
import { EventEmitter, Injectable, Output } from '@angular/core';
import { ToastController } from 'ionic-angular';
import moment from 'moment';

@Injectable()
export class GlobalProvider {

  headerGlobalHeight:number = 102;

	@Output() stopUjian: EventEmitter<any> = new EventEmitter();
	@Output() isLogin: EventEmitter<any> = new EventEmitter();
  isAuth:boolean = this.getStorage('is_login');

  constructor(
    public http     : HttpClient,
    public toastCtrl : ToastController
  ) {

  }

  config(name:string)
  {
    var config = {
      "versionApp"      : "3.3.1",
      "build"           : "1",
      "apiUrlMode"      : 1, // 0=>offline, 1=>online || default 1=online
      "apiUrlOffline"   : "http://1.2.4.28/AdminSmkv2/api/",
      "apiUrlOnline"    : "https://smkkartikatama.sch.id/api/",
      "requestTimeout"  : 30000, // 25second
    }

    return config[name];
  }

  getStorage(name: string)
  {
    let result = null;
    try {
      result = JSON.parse(window.localStorage.getItem(name));
    } catch (error) {
      result = null;
    }
    return result;
  }

  truncateTitle(content:string=''):string
  {
    let result = content;

    if(content !== undefined && content !== null && content !== '' && content.length > 30)
    {
      result = content.substring(0, 30) + ' ...';
    }
    return result;
  }

  setStorage(name: string, value: any)
  {
    return window.localStorage.setItem(name, JSON.stringify(value));
  }

  removeStorage(name: string)
  {
    return window.localStorage.removeItem(name);
  }

  clearStorage()
  {
    return window.localStorage.clear();
  }

  toast(msg:any="", duration:number=2000)
  {
    this.toastCtrl.create({
        message     : msg,
        duration    : duration,
    })
    .present();
  }

  convertDateGMT7(date)
  {
    if(date !== undefined && date !== null && date !== "")
    {
      return moment(date).utcOffset("+0700").format("DD-MM-YYYY");
    }
  }

  convertHours(date)
  {
    if(date !== undefined && date !== null && date !== "")
    {
      return moment(date).format("HH:mm");
    }
  }

  getTimer(ujian_uuid)
  {
    return this.getStorage("timer_"+ujian_uuid);
  }

  startTimer(time, ujian_uuid)
  {
    let getCurrentTimer = this.getStorage("timer_"+ujian_uuid);
    if(getCurrentTimer !== undefined && getCurrentTimer !== null)
    {
      let interval = setInterval(() => {
        this.setStorage("timer_"+ujian_uuid, getCurrentTimer--);

        if(getCurrentTimer == 0)
        {
          this.stopUjian.emit(ujian_uuid)
          this.stopTimer(this.getStorage("interval_"+ujian_uuid))
        }
      }, 60000);
      this.setStorage("interval_"+ujian_uuid, interval);
    }
    else
    {
      this.setStorage("timer_"+ujian_uuid, time);
      let interval = setInterval(() => {
        this.setStorage("timer_"+ujian_uuid, time--);
        if(time == 0)
        {
          this.stopUjian.emit(ujian_uuid)
          // let intervalID = this.getStorage("timer_"+ujian_uuid);
          this.stopTimer(this.getStorage("interval_"+ujian_uuid))
        }
      }, 60000);
      this.setStorage("interval_"+ujian_uuid, interval);
    }
  }

  stopTimer(intervalId)
  {
  console.log("🚀 ~ file: global.ts ~ line 118 ~ GlobalProvider ~ intervalId", intervalId)
    clearInterval(intervalId);
  }

}
