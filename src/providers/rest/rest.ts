import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { GlobalProvider } from '../global/global';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/timeout';

@Injectable()
export class RestProvider {

  apiUrl 			= this.global.config("apiUrlOnline");

  constructor(
    public http       : HttpClient,
    public global     : GlobalProvider,
  ) {
    var apiUrlMode 	= this.global.config("apiUrlMode");
		if(apiUrlMode == 0)
		{
			this.apiUrl = this.global.config("apiUrlOffline");
		}
  }

  getHeader()
	{
		let headers = new HttpHeaders({
			'Access-Control-Allow-Origin':'*',
			'Content-Type' : 'application/json',
		});

		return headers;
	}

  auth(criteria:any)
	{
		return new Promise( (resolve, reject) =>
		{
			this.http.post(this.apiUrl + 'authorize', criteria, {headers: this.getHeader()})
			.timeout(this.global.config("requestTimeout"))
			.subscribe(result => // response after API
			{
				resolve(result);
			}, (err) => // response before API
			{
				if (typeof err['name'] !== undefined && err['name'] == "HttpErrorResponse")
				{
					var resp1 = { status: 500, msg: "HttpErrorResponse", data: Array() };
					resolve(resp1);
				}
				else if (typeof err['name'] !== undefined && err['name'] == "TimeoutError")
				{
					var resp2 = { status: 500, msg: "TimeoutError", data: Array() };
					resolve(resp2);
				}
				else
				{
					resolve(err);
				}
			});
    });
	}

	listData(table_module:any, criteria:any)
	{
		return new Promise( (resolve, reject) =>
		{
			this.http.post(this.apiUrl + table_module + "/list", criteria, {headers: this.getHeader()})
			.timeout(this.global.config("requestTimeout"))
			.subscribe(result => // response after API
			{
				resolve(result);
			}, (err) => // response before API
			{
				if (typeof err['name'] !== undefined && err['name'] == "HttpErrorResponse")
				{
					var resp1 = { status: 500, msg: "HttpErrorResponse", data: Array() };
					resolve(resp1);
				}
				else if (typeof err['name'] !== undefined && err['name'] == "TimeoutError")
				{
					var resp2 = { status: 500, msg: "TimeoutError", data: Array() };
					resolve(resp2);
				}
				else
				{
					resolve(err);
				}
			});
    });
	}

  getAbsenStatus(criteria:any)
	{
		return new Promise( (resolve, reject) =>
		{
			this.http.post(this.apiUrl + "absensi", criteria, {headers: this.getHeader()})
			.timeout(this.global.config("requestTimeout"))
			.subscribe(result => // response after API
			{
				resolve(result);
			}, (err) => // response before API
			{
				if (typeof err['name'] !== undefined && err['name'] == "HttpErrorResponse")
				{
					var resp1 = { status: 500, msg: "HttpErrorResponse", data: Array() };
					resolve(resp1);
				}
				else if (typeof err['name'] !== undefined && err['name'] == "TimeoutError")
				{
					var resp2 = { status: 500, msg: "TimeoutError", data: Array() };
					resolve(resp2);
				}
				else
				{
					resolve(err);
				}
			});
    });
	}

	getDetail(table_module:any, criteria:any)
	{
		return new Promise( (resolve, reject) =>
		{
			this.http.post(this.apiUrl + table_module + "/detail", criteria, {headers: this.getHeader()})
			.timeout(this.global.config("requestTimeout"))
			.subscribe(result => // response after API
			{
				resolve(result);
			}, (err) => // response before API
			{
				if (typeof err['name'] !== undefined && err['name'] == "HttpErrorResponse")
				{
					var resp1 = { status: 500, msg: "HttpErrorResponse", data: Array() };
					resolve(resp1);
				}
				else if (typeof err['name'] !== undefined && err['name'] == "TimeoutError")
				{
					var resp2 = { status: 500, msg: "TimeoutError", data: Array() };
					resolve(resp2);
				}
				else
				{
					resolve(err);
				}
			});
    });
	}

  saveData(table_module:any, criteria:any)
	{
		return new Promise( (resolve, reject) =>
		{
			this.http.post(this.apiUrl + table_module + "/save", criteria, {headers: this.getHeader()})
			.timeout(this.global.config("requestTimeout"))
			.subscribe(result => // response after API
			{
				resolve(result);
			}, (err) => // response before API
			{
				if (typeof err['name'] !== undefined && err['name'] == "HttpErrorResponse")
				{
					var resp1 = { status: 500, msg: "HttpErrorResponse", data: Array() };
					resolve(resp1);
				}
				else if (typeof err['name'] !== undefined && err['name'] == "TimeoutError")
				{
					var resp2 = { status: 500, msg: "TimeoutError", data: Array() };
					resolve(resp2);
				}
				else
				{
					resolve(err);
				}
			});
    });
	}

  saveAbsen(criteria:any)
	{
		return new Promise( (resolve, reject) =>
		{
			this.http.post(this.apiUrl + "do-absen", criteria, {headers: this.getHeader()})
			.timeout(this.global.config("requestTimeout"))
			.subscribe(result => // response after API
			{
				resolve(result);
			}, (err) => // response before API
			{
				if (typeof err['name'] !== undefined && err['name'] == "HttpErrorResponse")
				{
					var resp1 = { status: 500, msg: "HttpErrorResponse", data: Array() };
					resolve(resp1);
				}
				else if (typeof err['name'] !== undefined && err['name'] == "TimeoutError")
				{
					var resp2 = { status: 500, msg: "TimeoutError", data: Array() };
					resolve(resp2);
				}
				else
				{
					resolve(err);
				}
			});
    });
	}

  getServerTime(criteria)
	{
		return new Promise( (resolve, reject) =>
		{
			this.http.post(this.apiUrl + "server-time", criteria, {headers: this.getHeader()})
			.timeout(this.global.config("requestTimeout"))
			.subscribe(result => // response after API
			{
				resolve(result);
			}, (err) => // response before API
			{
				if (typeof err['name'] !== undefined && err['name'] == "HttpErrorResponse")
				{
					var resp1 = { status: 500, msg: "HttpErrorResponse", data: Array() };
					resolve(resp1);
				}
				else if (typeof err['name'] !== undefined && err['name'] == "TimeoutError")
				{
					var resp2 = { status: 500, msg: "TimeoutError", data: Array() };
					resolve(resp2);
				}
				else
				{
					resolve(err);
				}
			});
    });
	}

  logout(criteria:any)
	{
		return new Promise( (resolve, reject) =>
		{
			this.http.post(this.apiUrl + "logout", criteria, {headers: this.getHeader()})
			.timeout(this.global.config("requestTimeout"))
			.subscribe(result => // response after API
			{
				resolve(result);
			}, (err) => // response before API
			{
				if (typeof err['name'] !== undefined && err['name'] == "HttpErrorResponse")
				{
					var resp1 = { status: 500, msg: "HttpErrorResponse", data: Array() };
					resolve(resp1);
				}
				else if (typeof err['name'] !== undefined && err['name'] == "TimeoutError")
				{
					var resp2 = { status: 500, msg: "TimeoutError", data: Array() };
					resolve(resp2);
				}
				else
				{
					resolve(err);
				}
			});
    });
	}

}
