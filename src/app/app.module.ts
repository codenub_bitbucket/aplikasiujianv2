import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { FileOpener } from '@ionic-native/file-opener';
import { FileTransfer } from '@ionic-native/file-transfer';
import { File } from '@ionic-native/file';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';
import { BerandaPage } from '../pages/beranda/beranda';
import { ExamPage } from '../pages/exam/exam';
import { ProfilePage } from '../pages/profile/profile';
import { WelcomePage } from '../pages/welcome/welcome';
import { ExamReviewPage } from '../pages/exam/review/exam_review';
import { ExamPreviewPage } from '../pages/exam/preview/exam_preview';
import { RestProvider } from '../providers/rest/rest';
import { GlobalProvider } from '../providers/global/global';
import { HttpClientModule } from '@angular/common/http';
import { ComponentsModule } from '../components/components.module';
import { ELearningPage } from '../pages/e-learning/e-learning';
import { FinishPage } from '../pages/exam/finish/finish';
import { ElearningDetailPage } from '../pages/e-learning/preview/detail';
import { BerandaParentPage } from '../pages/beranda-parent/beranda-parent';
import { ScreenOrientation } from '@ionic-native/screen-orientation';
import { CardPage } from '../pages/profile/card/card';
import { Base64ToGallery } from '@ionic-native/base64-to-gallery';
import { AndroidPermissions } from '@ionic-native/android-permissions';
import { DashboardPage } from '../pages/beranda-parent/dashboard/dashboard';
import { PengumumanPage } from '../pages/beranda-parent/pengumuman/pengumuman';
import { SettingPage } from '../pages/beranda-parent/setting/setting';
import { PengumumanPreviewPage } from '../pages/beranda-parent/pengumuman/pengumuman_preview/pengumuman_preview';
import { Camera } from '@ionic-native/camera';
import { AbsenPage } from '../pages/exam/absen/absen';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    LoginPage,
    BerandaPage,
    ExamPage,
    ProfilePage,
    ELearningPage,
    ElearningDetailPage,
    FinishPage,
    WelcomePage,
    ExamPreviewPage,
    ExamReviewPage,
    BerandaParentPage,
    CardPage,
    DashboardPage,
    PengumumanPage,
    SettingPage,
    PengumumanPreviewPage,
    AbsenPage,
  ],
  imports: [
    ComponentsModule,
    HttpClientModule, // handle rx.js
    BrowserModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    LoginPage,
    BerandaPage,
    ExamPage,
    ProfilePage,
    ELearningPage,
    ElearningDetailPage,
    FinishPage,
    WelcomePage,
    ExamPreviewPage,
    ExamReviewPage,
    BerandaParentPage,
    CardPage,
    DashboardPage,
    PengumumanPage,
    SettingPage,
    PengumumanPreviewPage,
    AbsenPage,
  ],
  providers: [
    ScreenOrientation,
    StatusBar,
    Base64ToGallery,
    SplashScreen,
    FileOpener,
    AndroidPermissions,
    FileTransfer,
    File,
    Camera,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    InAppBrowser,
    RestProvider,
    GlobalProvider
  ]
})
export class AppModule {}
